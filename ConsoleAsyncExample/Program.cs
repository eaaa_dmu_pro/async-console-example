﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleAsyncExample
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Task<int> AccessTheWebTask = AccessTheWebAsync();
            while (!AccessTheWebTask.IsCompleted)
            {
                Console.WriteLine(AccessTheWebTask.Status);
                Thread.Sleep(1000);
            }
            Console.WriteLine($"Lenght of page is: {AccessTheWebTask.Result}");
            Console.ReadLine();
        }

        private static async Task<int> AccessTheWebAsync()
        {
            HttpClient client = new HttpClient();

            Task<string> getStringTask = client.GetStringAsync("http://msdn.microsoft.com");

            DoIndependentWork();

            string urlContents = await getStringTask;

            return urlContents.Length;
        }

        private static void DoIndependentWork()
        {
            Console.WriteLine("Please wait while fetching data...");
        }
    }
}
